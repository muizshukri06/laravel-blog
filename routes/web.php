<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// GET : Show all blog posts
Route::get('/blog', [App\Http\Controllers\BlogPostController::class, 'index']);
// GET : Show a blog post
Route::get('/blog/{blogPost}', [App\Http\Controllers\BlogPostController::class, 'show']);
// GET : Show create post form
Route::get('/blog/create/post', [App\Http\Controllers\BlogPostController::class, 'create']);
// POST : Store created post in DB
Route::post('/blog/create/post', [App\Http\Controllers\BlogPostController::class, 'store']);
// GET : Show edit post form
Route::get('/blog/{blogPost}/edit', [App\Http\Controllers\BlogPostController::class, 'edit']);
// PUT : Put new edited post in DB
Route::put('/blog/{blogPost}/edit', [App\Http\Controllers\BlogPostController::class, 'update']);
// DELETE : Delete post from DB
Route::delete('blog/{blogPost}', [App\Http\Controllers\BlogPostController::class, 'destroy']);
