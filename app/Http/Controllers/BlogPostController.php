<?php

namespace App\Http\Controllers;

use App\Models\BlogPost;
use Illuminate\Http\Request;

class BlogPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Show all blog posts
        $posts = BlogPost::all();   // Fetch all blog posts from DB
        return view('blog.index', [
            'posts' => $posts
        ]); // Return the view with the fetched posts
        // return $posts;  // Return the fetched posts
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Show create post form
        return view('blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  Store created post into DB
        $newPost = BlogPost::create([
            'title' => $request->title, // Store post title
            'body' => $request->body,   // Store post body paragraph
            'user_id' => 1  // Store author ID
        ]);

        return redirect('blog/' . $newPost->id);    // Redirect to welcome page and create post
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BlogPost  $blogPost
     * @return \Illuminate\Http\Response
     */
    public function show(BlogPost $blogPost)
    {
        // Show a blog post
        return view('blog.show', [
            'post' => $blogPost,
        ]); // Return the view with a post
        // return $blogPost;   // Return a post
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BlogPost  $blogPost
     * @return \Illuminate\Http\Response
     */
    public function edit(BlogPost $blogPost)
    {
        //  Show edit post form
        return view('blog.edit', [
            'post' => $blogPost
        ]); // Return the edit post view with the post
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BlogPost  $blogPost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BlogPost $blogPost)
    {
        //  Update edited post into DB
        $blogPost->update([
            'title' => $request->title, // Update post title
            'body' => $request->body    // Update post body paragraph
        ]);

        return redirect('blog/' . $blogPost->id);   // Redirect to welcome page and edit post
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BlogPost  $blogPost
     * @return \Illuminate\Http\Response
     */
    public function destroy(BlogPost $blogPost)
    {
        //  Delete a post in DB
        $blogPost->delete();    // Delete a post

        return redirect('/blog');   // Redirect to welcome page
    }
}
