<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class BlogPostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence(10),  // Generate a dummy title
            'body' => $this->faker->paragraph(30),  // Generate a dummy body paragraph
            'user_id' => User::factory()    // Generate a dummy author ID
        ];
    }
}
